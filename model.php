<?php 
include('connexion.php');

class Model {
	protected $query;

	// La classe Result permet de stocker le tableau résultat de la requête ainsi que les index du tableau 
	protected $result_query;
	protected $field;

	function set_result_field($result, $field){
		$this->result_query = $result;
		$this->field = $field;
	}

	function get_result(){
		return $this->result_query;
	}

	function get_field(){
		return $this->field;
	}


	// Permet de réaliser une requête mise en premier paramètre et de donner une liste des fields selectionnées dans le second.
	function query_result($q, $list_field){
		global $pdo;
		$this->set_result_field($pdo->query($q), $list_field);
	}

	/*
	function get_musicien_by_genre($code_genre){
		$query = 'SELECT Code_Musicien, Nom_Musicien, Prénom_Musicien, Code_Pays, Code_Instrument 
					FROM Musicien
					WHERE Code_Genre=' . $code_genre;
	}

	function get_oeuvre_from_musicien($name){
		$my_result = new Result();
		$requete = 'SELECT Oeuvre.Titre_Oeuvre, Musicien.Nom_Musicien
						FROM Oeuvre
						JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre
						JOIN Musicien ON Musicien.Code_Musicien = Composer.Code_Musicien
						JOIN Genre ON Genre.Code_Genre = Musicien.Code_Genre
						JOIN Album ON Album.Code_Genre = Genre.Code_Genre
						WHERE Musicien.Nom_Musicien LIKE \'%A\'
						GROUP BY Oeuvre.Titre_Oeuvre, Musicien.Nom_Musicien
						ORDER BY 1';

		foreach ($pdo->query($requete) as $row) {
			echo $row['Titre_Oeuvre'] . ' ' . $row['Nom_Musicien']. '<br>';
		}
		$field = array(1 => 'Titre_Oeuvre', 'Nom_Musicien');

		$my_result.set_result_field($field, $pdo->query($requete));
	}

	function get_album_by_Oeuvre_by_Musicien($name){
		$query = 'SELECT Oeuvre.Titre_Oeuvre, Musicien.Nom_Musicien, Musicien.Prénom_Musicien, Album.Titre_Album
					FROM Oeuvre
					JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre
					JOIN Musicien ON Musicien.Code_Musicien = Composer.Code_Musicien
					JOIN Genre ON Genre.Code_Genre = Musicien.Code_Genre
					JOIN Album ON Album.Code_Genre = Genre.Code_Genre
					WHERE Musicien.Nom_Musicien LIKE \'' . $name . '\'
					ORDER BY 1';
	}

	function get_enregistrement_by_album_by_Oeuvre_by_Musicien($name, $oeuvre){
		$query = 'SELECT Oeuvre.Titre_Oeuvre, Musicien.Nom_Musicien, Musicien.Prénom_Musicien, Album.Titre_Album, Enregistrement.Titre
					FROM Oeuvre
					JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre
					JOIN Musicien ON Musicien.Code_Musicien = Composer.Code_Musicien
					JOIN Genre ON Genre.Code_Genre = Musicien.Code_Genre
					JOIN Album ON Album.Code_Genre = Genre.Code_Genre
					JOIN Disque ON Disque.Code_Album = Album.Code_Album
					JOIN Compositon_Disque ON Composition_Disque.Code_Disque = Disque.Code_Disque
					JOIN Enregistrement ON Enregistrement.Code_Morceau = Composition_Disque.Code_Morceau
					WHERE Musicien.Nom_Musicien LIKE \'' . $name . '\'
					AND Oeuvre.Titre_Oeuvre LIKE \'' . $oeuvre . '\'
					ORDER BY 1, 5';
	}
	*/
}
