<?php 
include('template_view.php');
top('Catalogue');
include('ModelOeuvre.php');
include('ModelMusicien.php');
$mO = new ModelOeuvre();
$mO->get_oeuvre_from_musicien('A');

$result_field = $mO->get_result();
$field_selected = $mO->get_field();

tableau_Oeuvre($result_field, $field_selected);

$mM = new ModelMusicien();
$mM->get_musicien();

$result_field = $mM->get_result();
$field_selected = $mM->get_field();
formulaire_autocompletion($result_field, $field_selected);
bot();
 ?>