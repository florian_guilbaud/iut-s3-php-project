<?php 
include ('model.php');

class ModelMusicien extends Model {
	function get_musicien(){
		$query = 'SELECT Code_Musicien, Nom_Musicien, Prénom_Musicien, Code_Pays, Code_Instrument 
					FROM Musicien';
		$field = array(1 => 'Code_Musicien', 'Nom_Musicien', 'Prénom_Musicien', 'Code_Pays', 'Code_Instrument');
		$this->query_result($query, $field);
	}
}
 ?>