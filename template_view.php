<?php 
function top($page_title) {
	echo '
	 <!DOCTYPE html>
					<html lang="fr">
					<head>
					  <meta charset="utf-8">
					  <meta http-equiv="X-UA-Compatible" content="IE=edge">
					  <meta name="viewport" content="width=device-width, initial-scale=1">
					  <meta name="description" content="">
					  <meta name="author" content="">

					  <title>'.$page_title.'</title>

					  <!-- Bootstrap core CSS -->
					  <link href="bootstrap.min.css" rel="stylesheet">
					  <link href="my_style.css" rel="stylesheet">
					  <link rel="stylesheet" href="jquery/jquery-ui.css">
					  <script src="jquery/jquery-1.10.2.js"></script>
					  <script src="jquery/jquery-ui.js"></script>

					  <!-- Custom styles for this template -->
					</head>

					<body>
						<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
							<div class="container">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand" href="#">Project name</a>
								</div>
								<div class="collapse navbar-collapse">
									<ul class="nav navbar-nav">
										<li><a href="index.php">Accueil</a></li>
										<li><a href="catalogue.php">Catalogue</a></li>
										<li><a href="propos.php">A propos</a></li>
										<li><a href="login.php">Connexion</a></li>
										<li><a href="signin.php">Inscription</a></li>';
		if(isset($login)){
			echo '						<li><a href="panier.php">Panier</a></li>
										<li><a href="profil.php">' . $login . '</a></li>';
		}
		echo '
									</ul>
								</div><!--/.nav-collapse -->
							</div>
						</div>
						<div class="container">';
}

function bot() {
	echo '</div><!-- /.container -->
			</body>
			</html>';
}

function form_login() {
	echo '<form class="form-inline" role="form">
			  <div class="form-group">
			    <div class="input-group">
			      <label class="sr-only" for="exampleInputEmail2">Adresse mail</label>
			      <div class="input-group-addon">@</div>
			      <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Entrer votre adresse mail">
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="sr-only" for="exampleInputPassword2">Mot de passe</label>
			    <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Mot de passe">
			  </div>
			  <div class="checkbox">
			    <label>
			      <input type="checkbox"> Se rappeler de moi
			    </label>
			  </div>
			  <button type="submit" class="btn btn-default">Connexion</button>
			</form>';
}

function form_signin() {
	echo '<form role="form">
			  <div class="form-group">
			    <label for="exampleInputEmail1">Votre adresse mail</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Rentrer votre adresse mail">
			  </div>
			  <div class="form-group">
			    <label for="password1">Entrer votre mot de passe</label>
			    <input type="password" class="form-control" id="password1" placeholder="Mot de passe">
			    <label for="password2">Répéter votre mot de passe</label>
			    <input type="password" class="form-control" id="password2" placeholder="Mot de passe">
			  </div>
			  <button type="submit" class="btn btn-default">Valider l\'inscription</button>
			</form>';
}

function tableau_Oeuvre($list_Oeuvre_Musicien, $list_field) {
	echo '<table class="table table-bordered" style="width:100%">
			  <tr>
			    <td class="info"><b>Musicien</b></td> 
			  </tr>';
	foreach ($list_Oeuvre_Musicien as $elem) {
		echo '<tr>';
		foreach ($list_field as $field) {
			echo '<td class="success"><center>' . $elem[$field] . '</center></td>';
		}
		echo '</tr>';
	}
	echo '</table>';
}

function formulaire_autocompletion($list_musicien, $list_field) {
	echo '<script>
		  $(function() {
		    var availableTags = [';
	foreach ($list_musicien as $elem) {
		foreach ($list_field as $field) {
			echo  '"'.$elem[$field].'",';
		}
	}
	echo '];
		    $( "#tags" ).autocomplete({
		      source: availableTags
		    });
		  });
		  </script>
		</head>
		<body>
		 
		<div class="ui-widget">
		  <label for="tags">Tags: </label>
		  <input id="tags">
		</div>';	    
}
