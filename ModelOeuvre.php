<?php
include ('connexion.php');
include ('model.php');
class ModelOeuvre extends Model {
	function get_oeuvre_from_musicien($name){
		$query = 'SELECT Musicien.Nom_Musicien
						FROM Oeuvre
						JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre
						JOIN Musicien ON Musicien.Code_Musicien = Composer.Code_Musicien
						JOIN Genre ON Genre.Code_Genre = Musicien.Code_Genre
						JOIN Album ON Album.Code_Genre = Genre.Code_Genre
						WHERE Musicien.Nom_Musicien LIKE \'%A\'
						GROUP BY Musicien.Nom_Musicien
						ORDER BY 1';
		$field = array(1 => 'Nom_Musicien');
		$this->query_result($query, $field);
	}
}
 ?>