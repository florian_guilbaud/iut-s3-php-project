<?php 
class ModelAlbum extends Model {
	function get_album_by_Oeuvre_by_Musicien($name){
		$query = 'SELECT Oeuvre.Titre_Oeuvre, Musicien.Nom_Musicien, Musicien.Prénom_Musicien, Album.Titre_Album
					FROM Oeuvre
					JOIN Composer ON Composer.Code_Oeuvre = Oeuvre.Code_Oeuvre
					JOIN Musicien ON Musicien.Code_Musicien = Composer.Code_Musicien
					JOIN Genre ON Genre.Code_Genre = Musicien.Code_Genre
					JOIN Album ON Album.Code_Genre = Genre.Code_Genre
					WHERE Musicien.Nom_Musicien LIKE \'' . $name . '%\'
					ORDER BY 1';
		$field = array(1 => 'Titre_oeuvre', 'Nom_Musicien', 'Prénom_Musicien', 'Titre_Album');
		return $this->query_result($query, $field);
	}
}
 ?>